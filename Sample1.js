const TajMahalIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'TajMahalIntent';
    },
    handle(handlerInput) {
         const data = require('./data/TajMahal.json');
        const template = require('./templates/TajMahal.json');
        const speakOutput = 'The Taj Mahal is an ivory-white marble mausoleum on the south bank of the Yamuna river in the Indian city of Agra. In 2007, it was declared a winner of the New7Wonders of the World (2000–2007) initiative.';
       if (supportsAPL(handlerInput)) {
             handlerInput.responseBuilder
             .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: template,
                datasources: data
            })
        };